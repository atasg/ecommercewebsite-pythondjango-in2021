from django.urls import path

from django.contrib.auth import views as auth_views

from . import views


urlpatterns = [
    path('',views.homepage , name = "homepage"),
    path('store/',views.store , name = "store"),
    path('banner_1/',views.banner_1 , name = "banner_1"),
    path('banner_2/',views.banner_2 , name = "banner_2"),
    path('banner_3/',views.banner_3 , name = "banner_3"),
    path('banner_4/',views.banner_4 , name = "banner_4"),
    path('about/',views.about , name = "about"),
    path('contact/',views.contact , name = "contact"),

    path('gizlilik_guvenlik/',views.gizlilik_guvenlik , name = "gizlilik_guvenlik"),
    path('iptal_iade_sartlari/',views.iptal_iade_sartlari , name = "iptal_iade_sartlari"),
    path('kisisel_veriler_politikasi/',views.kisisel_veriler_politikasi , name = "kisisel_veriler_politikasi"),
    path('mesafeli_satis_sozlesmesi/',views.mesafeli_satis_sozlesmesi , name = "mesafeli_satis_sozlesmesi"),

    path('login/',views.loginPage , name = "login"),
    path('logout/',views.logoutUser , name = "logout"),
    path('return_request/',views.return_request , name = "return_request"),


    path('account/',views.accountSettings , name = "account"),
    
    path('productdetails/<str:idProduct>/',views.productdetails , name = "productdetails"),
    path('cart/',views.cart , name = "cart"),
    path('checkout/',views.checkout , name = "checkout"),
    path('checkout_2/',views.checkout_2 , name = "checkout_2"),
    path('order_success/',views.order_success , name = "order_success"),

    path('update_item/',views.updateItem, name = "update_item"),
    path('process_order/',views.processOrder, name = "process_order"),

    path('reset_password/', 
    auth_views.PasswordResetView.as_view(template_name="store/password_reset.html"), 
    name="reset_password"),

    path('reset_password_sent/', 
    auth_views.PasswordResetDoneView.as_view(template_name="store/password_reset_sent.html"), 
    name="password_reset_done"),

    path('reset/<uidb64>/<token>/', 
    auth_views.PasswordResetConfirmView.as_view(template_name="store/password_reset_form.html"), 
    name="password_reset_confirm"),

    path('reset_password_complete/', 
    auth_views.PasswordResetCompleteView.as_view(template_name="store/password_reset_done.html"), 
    name="password_reset_complete"),
]