from django.shortcuts import render, redirect
from django.forms import inlineformset_factory
from django.http import HttpResponse
from django.http import JsonResponse
import json
import datetime
from django.views.decorators.csrf import csrf_exempt

from .models import *
from .utils import cookieCart, cartData, guestOrder, cookieProduct, getOrderInfo
from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm, CustomerForm
from django.contrib import messages

from django.contrib.auth import authenticate, login, logout

from django.contrib.auth.decorators import login_required

# Create your views here.

def homepage(request):
    data = cartData(request)

    cartItems = data['cartItems']

    meet1 = HomePageSlider.objects.get(title = 'meet1')
    slider1 = HomePageSlider.objects.get(title = 'slider1')
    slider2 = HomePageSlider.objects.get(title = 'slider2')
    slider3 = HomePageSlider.objects.get(title = 'slider3')
    meet2 = HomePageSlider.objects.get(title = 'meet2')

    products = Product.objects.all().filter(category = 'homepage')
    
    context = {'products':products, 'cartItems':cartItems, 'slider1':slider1, 'slider2':slider2,'slider3':slider3, 'meet1':meet1, 'meet2':meet2}

    return render(request, 'store/homepage.html', context)

def about(request):
    data = cartData(request)

    cartItems = data['cartItems']

    context = {'cartItems':cartItems}
    return render(request, 'store/about.html', context)

@csrf_exempt
def registerPage(request):
    data = cartData(request)

    cartItems = data['cartItems']

    if request.user.is_authenticated:
        return redirect('homepage')
    else:

        form = CreateUserForm()

        if request.method == 'POST_REGISTER':
            form = CreateUserForm(request.POST_REGISTER)
            if form.is_valid():
                user = form.save()
                user.username = user.email
                user.save()
                first_name = form.cleaned_data.get('first_name')
                last_name = form.cleaned_data.get('last_name')
                email = form.cleaned_data.get('email')

                Customer.objects.create(
                    user = user, name = first_name, surname = last_name, email = email,
                )

                messages.success(request, first_name + ' ' + last_name + ' için hesap oluşturuldu.')

                return redirect('login')


        context = {'form':form,'cartItems':cartItems}
        return render(request, 'store/register.html', context)

@csrf_exempt
def loginPage(request):
    
    data = cartData(request)

    cartItems = data['cartItems']

    if request.user.is_authenticated:
        return redirect('homepage')
    else:
        form = CreateUserForm()
            
        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                user = form.save()
                user.username = user.email
                user.save()
                first_name = form.cleaned_data.get('first_name')
                last_name = form.cleaned_data.get('last_name')
                email = form.cleaned_data.get('email')

                Customer.objects.create(
                    user = user, name = first_name, surname = last_name, email = email,
                )

                messages.success(request, first_name + ' ' + last_name + ' için hesap oluşturuldu.')

                return redirect('login')

        

        
        if request.method == 'POST':
            email = request.POST.get('email')
            password = request.POST.get('password')

            user = authenticate(request, username=email, password=password)

            if user is not None:
                login(request, user)
                return redirect('homepage')
            else:
                messages.info(request, 'Email veya şifre yanlış!')

        context = {'form':form,'cartItems':cartItems}
        return render(request, 'store/login.html', context)



def logoutUser(request):
    logout(request)
    return redirect('login')

@csrf_exempt
def accountSettings(request):
    data = cartData(request)

    cartItems = data['cartItems']

    customer = request.user.customer
    form = CustomerForm(instance=customer)

    orders = Order.objects.filter(customer = customer, complete=True)

    context = {'form': form, 'cartItems':cartItems, 'orders':orders}

    if request.method == 'POST':
        print("whjasd")
        form = CustomerForm(request.POST, instance=customer)
        print(request.POST)
        if form.is_valid():
            form.save()
            return redirect('account')


    return render(request, 'store/account_settings.html', context)

def contact(request):
    data = cartData(request)

    cartItems = data['cartItems']

    context = {'cartItems':cartItems}
    return render(request, 'store/contact.html', context)

def store(request):
    data = cartData(request)

    cartItems = data['cartItems']

    products = Product.objects.all()
    context = {'products':products, 'cartItems':cartItems}
    return render(request, 'store/store.html', context)

def banner_1(request):
    data = cartData(request)

    cartItems = data['cartItems']

    products = Product.objects.all().filter(banner = 'banner-1')
    context = {'products':products, 'cartItems':cartItems}
    return render(request, 'store/banner-1.html', context)

def banner_2(request):
    data = cartData(request)

    cartItems = data['cartItems']

    products = Product.objects.all().filter(banner = 'banner-2')
    context = {'products':products, 'cartItems':cartItems}
    return render(request, 'store/banner-2.html', context)

def banner_3(request):
    data = cartData(request)

    cartItems = data['cartItems']

    products = Product.objects.all().filter(banner = 'banner-3')
    context = {'products':products, 'cartItems':cartItems}
    return render(request, 'store/banner-3.html', context)

def banner_4(request):
    data = cartData(request)

    cartItems = data['cartItems']

    products = Product.objects.all().filter(banner = 'banner-4')
    context = {'products':products, 'cartItems':cartItems}
    return render(request, 'store/banner-4.html', context)

def productdetails(request, idProduct):

   # veri = cookieProduct(request)

   # productId = veri['productId']
   # product = Product.objects.get(id = productId)


    product = Product.objects.get(id = idProduct)
    data = cartData(request)
    cartItems = data['cartItems']

    context = {'product':product,'cartItems':cartItems}
    return render(request, 'store/productdetails.html', context)


def cart(request):
    data = cartData(request)

    cartItems = data['cartItems']
    order = data['order']
    items = data['items']

    context = {'items':items, 'order':order, 'cartItems':cartItems}
    return render(request, 'store/cart.html', context)

@csrf_exempt
def checkout(request): 
    data_2 = cartData(request)

    cartItems = data_2['cartItems']
    order = data_2['order']
    items = data_2['items']

    context = {'items':items, 'order':order, 'cartItems':cartItems}
    return render(request, 'store/checkout.html', context)


def mesafeli_satis_sozlesmesi(request):
    data = cartData(request)

    cartItems = data['cartItems']

    context = {'cartItems':cartItems}
    return render(request, 'store/mesafeli_satis_sozlesmesi.html', context)

def iptal_iade_sartlari(request):
    data = cartData(request)

    cartItems = data['cartItems']

    context = {'cartItems':cartItems}
    return render(request, 'store/iptal_iade_sartlari.html', context)

def gizlilik_guvenlik(request):
    data = cartData(request)

    cartItems = data['cartItems']

    context = {'cartItems':cartItems}
    return render(request, 'store/gizlilik_guvenlik.html', context)

def kisisel_veriler_politikasi(request):
    data = cartData(request)

    cartItems = data['cartItems']

    context = {'cartItems':cartItems}
    return render(request, 'store/kisisel_veriler_politikasi.html', context)


def updateItem(request):
    data = json.loads(request.body)
    productId = data['productId']
    action = data['action']
    print('Action:', action)
    print('Product:', productId)

    customer = request.user.customer
    product = Product.objects.get(id = productId)
    if action == 'add' or action == 'remove':
        order, created=Order.objects.get_or_create(customer = customer, complete = False)

        orderItem, created=OrderItem.objects.get_or_create(order = order, product = product)
    if action == 'show':
        productshown, created=ProductShown.objects.create(productId = productId, customer = customer)

        productshown.save()

    if action == 'add':
        orderItem.quantity = (orderItem.quantity + 1)
    elif action == 'remove':
        orderItem.quantity = (orderItem.quantity - 1)
    if action == 'add' or action == 'remove':
        orderItem.save()

        if orderItem.quantity <= 0:
            orderItem.delete()
        
    return JsonResponse('Item was added', safe=False)


@csrf_exempt

def processOrder(request):
    transaction_id = datetime.datetime.now().timestamp()
    date = datetime.datetime.now().date()
    data = json.loads(request.body)

    if request.user.is_authenticated:
        customer = request.user.customer
        order, created = Order.objects.get_or_create(customer=customer, complete=False)

    else:
        customer, order = guestOrder(request, data)

    total = float(data['form']['total'])
    order.transaction_id = transaction_id
    order.date_ordered_date = date

    if total == float(order.get_cart_total):
        order.complete = True
        order.total_price = float(order.get_cart_total)
    order.save()

    if order.shipping == True:
        ShippingAddress.objects.create(
                customer=customer,
                order=order,
                city=data['shipping']['city'],
                ilce=data['shipping']['ilce'],
                address=data['shipping']['address'],
            )

    return JsonResponse('Payment complete!', safe=False)


@csrf_exempt
def checkout_2(request):

    data_2 = cartData(request)

    cartItems = data_2['cartItems']
    order = data_2['order']
    items = data_2['items']

    date = datetime.datetime.now().date()
    time = datetime.datetime.now().time()

    name = request.POST.get('name',None)
    surname = request.POST.get('surname',None)
    phone = request.POST.get('phone',None)
    email = request.POST.get('email',None)
    city = request.POST.get('city',None)
    ilce = request.POST.get('ilce',None)
    address = request.POST.get('address',None)
    
    context = {'time':time, 'date':date, 'name':name, 'surname':surname, 'phone':phone,'email':email, 'city':city, 'ilce':ilce, 'address':address, 'items':items, 'order':order, 'cartItems':cartItems }
    return render(request, 'store/checkout_2.html', context)


def order_success(request):

    context = {}
    return render(request, 'store/order_success.html', context)

def return_request(request):

    try:
        return_request = json.loads(request.COOKIES['return_request'])
    except:
        return_request = {}

    for i in return_request:
        return_order_id = i
        return_product_id = return_request[i]["product_id"]
    
    print(return_product_id)

    product = Product.objects.get(id = return_product_id)
    context = {'product':product, 'return_order_id':return_order_id, 'return_product_id':return_product_id}
    return render(request, 'store/return_request.html', context)



    