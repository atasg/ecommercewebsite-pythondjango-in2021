import json
from .models import *

def getOrderInfo(request):
    try:
        userFormData = json.loads(request.COOKIES['userFormData'])
        shippingInfo = json.loads(request.COOKIES['shippingInfo'])
    except:
        userFormData = {}
        shippingInfo = {}
    
    return {'userFormData':userFormData, 'shippingInfo':shippingInfo}

def cookieProduct(request):
    print('it works')

    try:
        productlist = json.loads(request.COOKIES['productlist'])
    except:
        productlist = {}
    
    for i in productlist:
        try:
            productId = i
        except:
            pass
    return {'productId':productId}  

def cookieCart(request):
    print('selaaaaaaam')

    try:
        cart = json.loads(request.COOKIES['cart'])
    except:
        cart = {}
    
    print('Cart:', cart)
    items = []
    order = {'get_cart_total':0, 'get_cart_items':0, 'shipping': False}
    cartItems = order['get_cart_items']

    for i in cart:
        try:
            cartItems += cart[i]["quantity"]

            product = Product.objects.get(id=i)
            total = (product.price * cart[i]["quantity"])

            order['get_cart_total'] += total
            order['get_cart_items'] += cart[i]["quantity"]

            item = {
                'product':{
                    'id':product.id,
                    'name':product.name,
                    'price':product.price,
                    'imageURL':product.imageURL,
                    },
                'quantity':cart[i]["quantity"],
                'get_total':total,
                }
            items.append(item)

            if product.digital == False:
                order['shipping'] = True
        except:
            pass
    return {'cartItems':cartItems, 'order':order, 'items':items}

def cartData(request):
    print('helooooooooooooooooo')
    if request.user.is_authenticated:
        customer = request.user.customer
        order, created = Order.objects.get_or_create(customer=customer, complete=False)
        items = order.orderitem_set.all()
        cartItems = order.get_cart_items
    else:
        cookieData = cookieCart(request)
        cartItems = cookieData['cartItems']
        order = cookieData['order']
        items = cookieData['items']
    return {'cartItems':cartItems, 'order':order, 'items':items}



def guestOrder(request, data):
    print('User is not logged in..')

    print('COOKIES:', request.COOKIES)
    name = data['form']['name']
    surname = data['form']['surname']
    phone = data['form']['phone']
    email = data['form']['email']

    cookieData = cookieCart(request)
    items = cookieData['items']

    customer, created = Customer.objects.get_or_create(
        email=email,  
        )
    customer.name = name
    customer.surname = surname
    customer.phone = phone
    customer.save()

    order = Order.objects.create(
        customer=customer,
        complete=False,
        )

    for item in items:
        product = Product.objects.get(id=item['product']['id'])

        orderItem = OrderItem.objects.create(
            product=product,
            order=order,
            quantity=item['quantity']
            )
    return customer, order
