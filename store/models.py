from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from phonenumber_field.modelfields import PhoneNumberField 
from django import forms

# Create your models here.

class Customer(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=200, null = True)
    surname = models.CharField(max_length=200, null = True)
    phone = PhoneNumberField( null = True, blank = True)
    email = models.CharField(max_length=200, null = True)
    date_created = models.DateTimeField(auto_now_add=True, null = True)

    def __str__(self):
        return str(self.email)

class Product(models.Model):
    name = models.CharField(max_length=200, null = True)
    price = models.DecimalField(max_digits=9,decimal_places=2)
    digital = models.BooleanField(default=False, null=True, blank = False)
    featured = models.ImageField(null = True, blank=True)
    description = models.TextField(null = True, blank = True)
    category = models.CharField(max_length=200, null=True, blank=True)
    banner = models.CharField(max_length=200, null=True, blank=True)
 
    def __str__(self):
        return self.name

    @property
    def imageURL(self):
        try:
            url = self.featured.url
        except:
            url = ''
        return url


class Image(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    image = models.ImageField()

    def __str__(self):
        return self.name

    @property
    def imageURL(self):
        try:
            url = self.image.url
        except:
            url = ''
        print('URL:', url)
        return url


class Order(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, null = True,blank = True)
    date_ordered = models.DateTimeField(auto_now_add=True)
    date_ordered_date = models.DateField(auto_now_add=True)
    complete = models.BooleanField(default=False)
    transaction_id = models.CharField(max_length=100, null = True)
    total_price = models.DecimalField(max_digits=9,decimal_places=2, null=True, blank=True)

    def __str__(self):
        return str(self.id)

    @property
    def shipping(self):
        shipping = False
        orderitems = self.orderitem_set.all()
        for i in orderitems:
            if i.product.digital == False:
                shipping = True
        return shipping 
    

    @property
    def get_cart_total(self):
        orderitems = self.orderitem_set.all()
        total = sum([item.get_total for item in orderitems])
        return total

    @property
    def get_cart_items(self):
        orderitems = self.orderitem_set.all()
        total = sum([item.quantity for item in orderitems])
        return total
        

class OrderItem(models.Model):
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null = True)
    order = models.ForeignKey(Order, on_delete = models.SET_NULL, null=True)
    quantity = models.IntegerField(default=0, null=True, blank=True)
    date_added= models.DateTimeField(auto_now_add=True)

    @property 
    def get_total(self):
        total = self.product.price * self.quantity
        return total

class ProductShown(models.Model):
    productId = models.CharField(max_length=200, null = True)
    customer = models.ForeignKey(Customer, on_delete=models.SET_NULL,blank=True, null= True)
    datetime_added= models.DateTimeField(auto_now = True)
    

class ShippingAddress(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.SET_NULL,blank=True, null= True)
    order = models.ForeignKey(Order, on_delete=models.SET_NULL,blank=True, null=True)
    address = models.CharField(max_length=200, null=False) 
    city = models.CharField(max_length=200, null=False)
    ilce = models.CharField(max_length=200, null=False)
    date_added=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.address

class RatingAdmin(admin.ModelAdmin):
    readonly_fields = ('datetime_added',)


class HomePageSlider(models.Model):
    title = models.CharField(max_length=100, blank=False )
    description = models.TextField(max_length=700, blank = False)
    image = models.ImageField(blank=False)

    def __str__(self):
        return self.title

    @property
    def imageURL(self):
        try:
            url = self.image.url
        except:
            url = ''
        return url
