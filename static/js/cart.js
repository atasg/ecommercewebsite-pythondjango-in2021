var updateBtns = document.getElementsByClassName('update-cart')

var returnBtns = document.getElementsByClassName('get-return-info')

for(i = 0; i<returnBtns.length; i++) {
    returnBtns[i].addEventListener('click', function(){
        var product_id = this.dataset.product
        var order_id = this.dataset.order
        console.log('product:', product_id, 'order:', order_id)

        return_request={}
        if(return_request[order_id] == undefined){
            return_request[order_id] = {'product_id':product_id}
        }
        console.log('return_request', return_request)
        document.cookie = 'return_request=' + JSON.stringify(return_request) + ";domain=;path=/"
    })
}

for(i = 0; i<updateBtns.length; i++) {
    updateBtns[i].addEventListener('click', function(){
        var productId = this.dataset.product
        var action = this.dataset.action
        console.log('productId:', productId, 'Action:', action)
        
        if(action == 'show'){
            addCookieProductDetail(productId)
            updateUserOrder(productId, action)
        }else{
            console.log('USER:', user)
            if(user == 'AnonymousUser'){
                addCookieItem(productId, action)
            }else{
                updateUserOrder(productId, action)
            }
        }
        

    })
}

function addCookieProductDetail(productId){
    productlist = {}
    console.log('productlist was created!')
    if(productlist[productId] == undefined){
        productlist[productId] = {'show':1}
    }

    console.log('Productlist', productlist)
    document.cookie = 'productlist=' + JSON.stringify(productlist) + ";domain=;path=/"
    location.reload()
}

function addCookieItem(productId, action){
    console.log('Not logged in.. ')

    if (action =='add'){
        if (cart[productId] == undefined){
            cart[productId] = {'quantity':1}
        }else{
            cart[productId]['quantity'] += 1
        }
    }
    
    if (action == 'remove'){
        cart[productId]['quantity'] -= 1

        if(cart[productId]['quantity'] <= 0){
            console.log('Remove item')
            delete cart[productId];
        }
    }

    console.log('Cart:', cart)
    document.cookie = 'cart=' + JSON.stringify(cart) + ";domain=;path=/"
    location.reload()
}

function updateUserOrder(productId, action){
    console.log('User is logged in, sending data...')

    var url ='/update_item/'

    fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': csrftoken,
        },
        body:JSON.stringify({'productId': productId,'action': action})
    })
    .then((response)=>{
        return response.json();
    })
    .then((data)=> {
        console.log('data:', data)
        location.reload()
    })
}

