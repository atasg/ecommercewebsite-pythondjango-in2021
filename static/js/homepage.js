/* first slider */ 


$('.slider-one')
.not(".slick-initialized")
.slick({
    autoplay: true,
    autoplaySpeed:4000,
    dots: true,
    prevArrow:".site-slider .slider-btn .prev",
    nextArrow:".site-slider .slider-btn .next",
});

/* second slider */ 


$('.slider-two')
.not(".slick-initialized")
.slick({
    prevArrow:".site-slider-two .prev",
    nextArrow:".site-slider-two .next",
    slidesToShow:5,
    slidesToScroll:1,
    autoplay: true,
    autoplaySpeed:3000,
});

/* homepage product slider */

$('.slider_three')
.not(".slick-initialized")
.slick({  
    dots: true,
    customPaging: function(slider_three, i) {
        return '<button class="tab">' + $(slider_three.$slides[i]).attr('title') + '<i class="fa fa-sort-asc"></i></button>';
    },
    arrows: false,
    slidesToShow:1,
    slidesToScroll:1,
});

// asdasdasdad //
const navSlide = () =>{
    const burger = document.querySelector('.burger');
    const nav = document.querySelector('.page-slide');
    const navLinks = document.querySelectorAll('.nav-links li');
    const navLinksUser = document.querySelectorAll('.nav-links-user li');
    const page = document.querySelectorAll('.container-block-content');
    const footer = document.querySelectorAll('.box-footer');
    const logoContainer = document.querySelectorAll('.logo-container');
    
    burger.addEventListener('click', ()=>{
    // toggle nav
    
        nav.classList.toggle('nav-active');

        //animate links 
        navLinks.forEach((link, index)=>{
            if(link.style.animation){
                link.style.animation = ''
            }else {
                link.style.animation = `navLinkFade 0.5s ease forwards ${index / 5 + 0.4}s`;
            }
        });

        navLinksUser.forEach((link, index)=>{
            if(link.style.animation){
                link.style.animation = ''
            }else {
                link.style.animation = `navLinkFade 0.5s ease forwards ${index / 5 + 0.4}s`;
            }
        });

        page.forEach((link, index)=>{
            if(link.style.animation){
                link.style.animation = ''
            }else {
                link.style.animation = `pageFade 0.5s ease forwards ${index / 5 + 0.4}s`;
            }
        });

        logoContainer.forEach((link, index)=>{
            if(link.style.animation){
                link.style.animation = ''
            }else {
                link.style.animation = `pageFade 0.5s ease forwards ${index / 5 + 0.4}s`;
            }
        });

        footer.forEach((link, index)=>{
            if(link.style.animation){
                link.style.animation = ''
            }else {
                link.style.animation = `pageFade 0.5s ease forwards ${index / 5 + 0.4}s`;
            }
        });

        //burger animation 
        burger.classList.toggle('toggle');
    });
    
    
}
navSlide();